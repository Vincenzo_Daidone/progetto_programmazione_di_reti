#
#               PROGETTO DI RETI DI TELECOMUNICAZIONE - MATRICOLA 843915
#                                    TRACCIA 1
#                              Code for the Server

import socket
import time

# "mentre l’interfaccia che parla con il server ha indirizzo ip
# appartenente alla classe 10.10.10.0/24, classe a cui appartiene anche l’IP address del server
# centrale."

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('localhost', 8000))
server.listen(2)

server_ip = "10.10.10.10"
server_mac = "00:00:AA:0B:22:FF"
router_mac = "05:10:0A:CB:24:EF"

# Loop while until connect to the server
while True:
    print("[*] Server statu: online")
    routerConnection, address = server.accept()

    if routerConnection is not None:
        print("[*] Connected with: ")
        print(routerConnection, address)
        break

# While loop for analyzing the received message
while True:

    try:
        received_message = routerConnection.recv(1024)

        # opening the packet received
        received_message = received_message.decode("utf-8")
        source_mac = received_message[0:17]
        destination_mac = received_message[17:34]
        source_ip = received_message[34:45]
        destination_ip = received_message[45:56]
        message = received_message[56:]

        # Extrapolation of data in the message
        data = message.split("\n")

        # Calculating elapsed time
        time_elapsed = str(round(time.time() - float(message.split("\n")[-1]), 3))
        print("[*] Time for the packet to arrive: " + time_elapsed + " sec \n")

        print(data[0] + " - " + data[1] + " - " + data[2] + " - " + data[3])

    except Exception as data:
        print(Exception, ":", data)
        print("[*] Ops, something went wrong")

server.close()

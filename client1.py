#
#               PROGETTO DI RETI DI TELECOMUNICAZIONE - MATRICOLA 843915
#                                    TRACCIA 1
#                              Code for the Client1

import socket as sk
import time

# I 4 Dispositivi IoT hanno un indirizzamento appartenente ad una rete di Classe C del tipo 192.168.1.0/24

temp = "24"
umid = "58"


def get_temp():
    return temp


def get_umid():
    return umid


def get_time():
    current_time = time.localtime()
    return time.strftime("%H:%M", current_time)


# Define the Client1 ip and mac address
client1_ip = "192.168.1.75"
client1_mac = "32:04:0A:FF:14:CC"
router_mac = "05:10:0A:CB:24:EF"
gateway = ("localhost", 8200)

# Define the destination server
destination_server_ip = "10.10.10.10"
server_mac = "00:00:AA:0B:22:FF"

# Create the socket
socket = sk.socket(sk.AF_INET, sk.SOCK_DGRAM)

# Define the message to send
f = open("detection1.txt", "w")
f.write(client1_ip + "\n" + get_time() + "\n" + get_temp() + "\n" + get_umid())
f.close()

fr = open("detection1.txt", "r")
file_message = fr.read().split()

message = ""
for data in file_message:
    message += data + "\n"

message += str(time.time())

print("The message sent is: \n" + message)

try:
    # invio il messaggio
    print('sending data...')
    time.sleep(2)  # waiting 2 sec before sending message

    ethernet_header = ""
    IP_header = ""

    source_ip = client1_ip
    IP_header = IP_header + source_ip + destination_server_ip

    source_mac = client1_mac
    destination_mac = router_mac
    ethernet_header = ethernet_header + source_mac + destination_mac

    packet = ethernet_header + IP_header + message

    socket.sendto(bytes(packet, "utf-8"), gateway)

except Exception as info:
    print(info)
finally:
    print('closing socket')
    socket.close()

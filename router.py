#
#               PROGETTO DI RETI DI TELECOMUNICAZIONE - MATRICOLA 843915
#                                    TRACCIA 1
#                           Code for the Router/Gateway
import socket
import sys
import time

# Il Gateway ha due interfacce di rete: quella verso i dispositivi il cui IP Address appartiene alla
# stessa network dei dispositivi mentre l’interfaccia che parla con il server ha indirizzo ip
# appartenente alla classe 10.10.10.0/24, classe a cui appartiene anche l’IP address del server
# centrale.

router_UDP_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
router_UDP_socket.bind(("localhost", 8200))

router_TCP_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

router_ip = "10.10.10.25"
router_mac = "05:10:0A:CB:24:EF"

server = ("localhost", 8000)

arp_table_mac = dict()
dataBuffer = []

client1_ip = "192.168.1.2"
client1_mac = "32:04:0A:FF:14:CC"

server_ip = "10.10.10.10"
server_mac = "00:00:AA:0B:22:FF"

client1 = None

while True:
    while client1 is None:

        print('\n\r waiting to receive all messages...')
        # waiting to receive
        data, address = router_UDP_socket.recvfrom(1024)

        if client1 is None:
            client1 = address
            dataBuffer.append(data.decode("utf-8"))
            print('received %s bytes from %s' % (len(data), address))
            # print('[*] Data received are: %s', data)

        # Extrapolation of the time data from the message
        message = data.decode()[57:]

        time_elapsed = str(round(time.time() - float(message.split("\n")[-1]), 3))

        # Print in console the time elapsed for the packet to arrive
        print("[*] Time for packet transmission: " + time_elapsed + " sec \n")

    print("All messages have been received")
    print("Connecting with the server... \n")

    # Populating the arp table of the router
    arp_table_mac = {client1_ip: client1, server_ip: server_mac}

    client1 = None

    # Trying to connect to the server
    try:
        router_TCP_socket.connect(server)
    except Exception as data:
        print(Exception, ":", data)
        print("[*ERROR*] Server unreachable.\r\n")
        sys.exit(0)

    for data in dataBuffer:

        source_mac = data[0:17]
        destination_mac = data[17:34]
        source_ip = data[34:46]
        destination_ip = data[46:57]
        message = data[57:]

        # inserisco un nuovo dato time() per calcolare il tempo trascorso dall'invio del pacchetto alla ricezione
        message.split("\n")[-1] = str(time.time())

        print("[*] Source MAC address: {source_mac} \n [*] Destination MAC address: {destination_mac}".format(
            source_mac=source_mac, destination_mac=destination_mac))
        print("\n[*] Source IP address: {source_ip} \n [*] Destination IP address: {destination_ip}".format(source_ip=source_ip,
                                                                                                  destination_ip=destination_ip))
        print("\n[*] Message from " + source_ip + ":  \n" + message)

        # Replacing destination MAC address(next hop) and source ip
        ethernet_header = router_mac + arp_table_mac[destination_ip]
        IP_header = router_ip + destination_ip
        packet = ethernet_header + IP_header + message

        router_TCP_socket.send(bytes(packet, "utf-8"))

        time.sleep(2)
        print("All messages have been sent")

    # Remove data in buffer
    dataBuffer = []


router_TCP_socket.close()
